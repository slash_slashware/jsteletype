// **** API

/**
 * @constructor
 */
function JSTT (width, height, canvasId, virtualKeyboardId) {
	this.stack = new Array();
	this.textLines = new Array();
	this.width = width;
	this.height = height;
	for (var i = 0; i < this.height; i++){
		this.textLines[i] = '';
	}
	this.isPrinting = false;
	this.y = 0;
	this.x = 0;
	this.baseHeadPosition = 120;
	this.returnCarriageDelay = 25;
	this.keyboardPollingInterval = 300;
	this.printCharacterDelay = 25;
	this.activateReturnCarriageAnim = true;
	this.tabSize = 8;
	this._initCanvas(canvasId);
	this._initKeyboard();
	if (virtualKeyboardId){
		this._initVirtualKeyboard(virtualKeyboardId);
	}
	this.autoUpperCase = true;
	this.soundsEnabled = true;
}

JSTT.prototype.printLine = function (text) {
	this.print(text);
	this.carriageReturn();
};
 
JSTT.prototype.print = function (text) {
	for (var i = 0; i < text.length; i++){
		this._pushChar(text.charAt(i));
	}
};

JSTT.prototype.carriageReturn = function () {
	this._pushChar('\n');
};

// **** Private methods

// *** Buffering logic
JSTT.prototype._pushChar = function (c) {
	if (!this._isLoadingComplete()){
		var thus = this;
		setTimeout(function(){thus._pushChar(c);}, 1000);
		return;
	}
	this.stack.push(c);
	if (!this.isPrinting){
		this._doPrint();
	}
};

JSTT.prototype._doPrint = function () {
	if (this.returnCarriageAnim > this.baseHeadPosition - 230){
		var thus = this;
		setTimeout(function(){thus._doPrint();}, this.returnCarriageDelay);
		return;
	}
	var c = this.stack.shift();
	if (!c){
		this.isPrinting = false;
		return;
	}
	this.isPrinting = true;
	if (c === '\n'){
		this._carriageReturn();
	} else if (c == '\t'){
		var spaces = this.tabSize - (this.y % this.tabSize);
		this.y += spaces;
		this.textLines[0] += '                    '.substring(0,spaces);
	} else {
		this.y++;
		if (this.y > this.width){
			this._carriageReturn();
		}
		this._appendToLine(c);
	}
	var thus= this;
	setTimeout(function(){thus._doPrint();},this.printCharacterDelay);
};

JSTT.prototype._appendToLine = function(c){
	this.textLines[0] += c;
	if (c !== ' '){
		this._playKeyClick();
		this.drumBounce = 10;
	}
};

JSTT.prototype._carriageReturn = function () {
	if (this.activateReturnCarriageAnim)
		this.returnCarriageAnim = this.y * this.fontWidth + this.baseHeadPosition - 230;
	this.y = 0;
	// Shift all existing lines downward
	for (var i = this.height - 1; i > 0; i--){
		this.textLines[i] = this.textLines[i-1];
	}
	this.textLines[0] = '';
	this._playCarriageReturn();
};


// *** Display
JSTT.prototype._initCanvas = function(canvasId){
	this.images = {};
	this.sounds = {};
	this.canvas = document.getElementById(canvasId);
	this.ctx = this.canvas.getContext("2d");
	this.ctx.font="10pt PDP11,monospace";
	this.fontWidth = this.ctx.measureText("X").width;
	this._loadImages();
	this._loadSounds();
	var thus= this;
	setInterval(function(){thus._updateCanvas();}, 30);
};

JSTT.prototype._isLoadingComplete = function(){
	return this._areImagesReady() && this._areSoundsReady();
};

JSTT.prototype._updateCanvas = function(){
	if (!this._isLoadingComplete()){
		//Kram: We can add a loading screen here if you want
		this.ctx.fillStyle="#000000";
		this.ctx.fillRect(0,0,865,480);
		this.ctx.fillStyle="#FFFFFF";
		this.ctx.fillText("JSTELETYPE", 20, 20);
		this.ctx.fillText("LOADING... PLEASE WAIT.", 20, 40);
		return;
	}
	this.ctx.drawImage(this.images.base, 0, 0);
	
	this.ctx.fillStyle="#444444";
	for (var i = 0; i < this.textLines.length; i++){
		this.ctx.fillText(this.textLines[i],this.baseHeadPosition,250 - i *20);
	}
	this.ctx.drawImage(this.images.glass, 14, 114);
	if (this.returnCarriageAnim > this.baseHeadPosition - 230){
		this.ctx.drawImage(this.images.head, this.returnCarriageAnim, 250);
		this.ctx.drawImage(this.images.drum, this.returnCarriageAnim + 180, 250 - this.drumBounce);
		this.returnCarriageAnim -= 30;
	} else {
		this.ctx.drawImage(this.images.head, this.y*this.fontWidth + this.baseHeadPosition - 230, 250);
		this.ctx.drawImage(this.images.drum, this.y*this.fontWidth + this.baseHeadPosition - 230 + 180, 250 - this.drumBounce);
	}
	
	if (this.drumBounce > 0){
		this.drumBounce -= 2;
	}
	
	this.ctx.drawImage(this.images.top, 0, 0);
};

JSTT.prototype._areImagesReady = function(){
	for (var i in this.images){
		if (!this.images[i].ready)
			return false;
	}
	this.fontWidth = this.ctx.measureText("X").width;
	return true;
};

JSTT.prototype._loadImages = function(){
	this.images.base = this._loadImage("img/base.png");
	this.images.glass = this._loadImage("img/glass.png");
	this.images.top = this._loadImage("img/top.png");
	this.images.head = this._loadImage("img/head.png");
	this.images.drum = this._loadImage("img/drum.png");
};

JSTT.prototype._loadImage = function(url){
	var img = new Image();
	
	img.src = url;
	img.ready = false;
	
	this.addEvent(img, "load", function(){
		img.ready = true;
	});
	
	return img;
};

// *** Sounds
JSTT.prototype._areSoundsReady = function(){
	for (var i in this.keyClickChannels){
		if (!this.keyClickChannels[i].ready)
			return false;
	}
	if (!this.sounds.carriageReturn.ready)
		return false;
	return true;
};

JSTT.prototype._loadSounds = function(){
	this.sounds.carriageReturn = this._loadSound("wav/typewriter-line-break-1.wav");
	this.keyClickChannels = new Array();
	this.soundIndex = 0;
	for (var i = 0; i < 40; i++){
		this.keyClickChannels[i] = this._loadSound("wav/typewriter-key-1.wav");
	}
};

JSTT.prototype._loadSound = function(url){
	var audio = new Audio();
	audio.src = url;
	this.addEvent(audio, "canplaythrough", function(){
		audio.ready = true;
	});
	return audio;
};

JSTT.prototype._playKeyClick = function(){
	if (!this.soundsEnabled)
		return;
	this.keyClickChannels[this.soundIndex].play();
	this.soundIndex++;
	if (this.soundIndex == 40)
		this.soundIndex = 0;
};

JSTT.prototype._playCarriageReturn = function(){
	if (!this.soundsEnabled)
		return;
	this.sounds.carriageReturn.play();
};

// *** Keyboard
JSTT.prototype.inkey = function (onComplete) {
	if (this.inkeyComplete){
		this.inkeyComplete = false;
		if (this.autoUpperCase && this.inkeyBuffer)
			onComplete(this.inkeyBuffer.toUpperCase());
		else
			onComplete(this.inkeyBuffer);
		this.inkeyBuffer = '';
	} else {
		var thus = this;
		setTimeout(function(){thus.inkey(onComplete);}, this.keyboardPollingInterval);
	}
};

JSTT.prototype._initKeyboard = function(){
	var thus = this;
	this.addEvent(document, 'keypress', function(e){
		thus._keyPressed(e);
	});
	this.inkeyComplete = false;
	this.inkeyBuffer = '';
};

JSTT.prototype._keyPressed = function(event){
	if (event.keyCode == 13){
		this._pressKey('\n');
	} else {
		var chara = String.fromCharCode(event.charCode);
		this._pressKey(chara);
	}
}

JSTT.prototype._pressKey = function(chara){
	if (chara == '\n'){
		this._pushChar('\n');
		this.inkeyComplete = true;
	} else {
		this._pushChar(chara);
		this.inkeyBuffer += chara;
	}
}

// *** Virtual Keyboard
JSTT.prototype._initVirtualKeyboard = function(virtualKeyboardId){
	var virtualKeyboard = document.getElementById(virtualKeyboardId);
	var keyboardImage = new Image();
	keyboardImage.src = "img/keyboard.png";
	keyboardImage.useMap = "#keyboardmap";
	keyboardImage.style.width = "100%";
	virtualKeyboard.appendChild(keyboardImage); 
	var keyboardmap = document.createElement("MAP");
	keyboardmap.name = "keyboardmap";
	virtualKeyboard.appendChild(keyboardmap); 
	var COORDS = [
{ coords:"71,41,125,90", key:"1"},
{ coords:"129,39,184,91", key:"2"},
{ coords:"187,40,236,91", key:"3"},
{ coords:"242,39,294,91", key:"4"},
{ coords:"295,36,347,91", key:"5"},
{ coords:"351,37,405,91", key:"6"},
{ coords:"408,39,462,91", key:"7"},
{ coords:"466,36,519,91", key:"8"},
{ coords:"521,37,575,91", key:"9"},
{ coords:"579,35,629,91", key:"0"},
{ coords:"634,36,686,89", key:"*"},
{ coords:"692,35,744,90", key:"="},
{ coords:"110,95,158,147", key:"Q"},
{ coords:"162,96,218,147", key:"W"},
{ coords:"218,94,274,146", key:"E"},
{ coords:"269,95,326,146", key:"R"},
{ coords:"323,96,380,147", key:"T"},
{ coords:"381,99,436,147", key:"Y"},
{ coords:"436,93,491,147", key:"U"},
{ coords:"495,92,546,147", key:"I"},
{ coords:"550,95,602,147", key:"O"},
{ coords:"604,93,657,146", key:"P"},
{ coords:"714,91,800,146", key:"\n"},
{ coords:"133,149,184,195", key:"A"},
{ coords:"188,147,234,195", key:"S"},
{ coords:"237,147,289,197", key:"D"},
{ coords:"292,147,341,198", key:"F"},
{ coords:"343,147,394,196", key:"G"},
{ coords:"398,147,447,196", key:"H"},
{ coords:"450,147,501,199", key:"J"},
{ coords:"504,147,554,198", key:"K"},
{ coords:"558,149,609,197", key:"L"},
{ coords:"612,149,666,197", key:"+"},
{ coords:"164,196,216,254", key:"Z"},
{ coords:"219,199,266,257", key:"X"},
{ coords:"268,201,317,248", key:"C"},
{ coords:"274,252,617,298", key:" "},
{ coords:"320,201,370,246", key:"V"},
{ coords:"373,199,425,248", key:"B"},
{ coords:"428,198,476,248", key:"N"},
{ coords:"480,199,529,251", key:"M"},
{ coords:"532,199,580,251", key:","},
{ coords:"582,199,632,251", key:"."},
{ coords:"635,199,685,251", key:"?"}
	      	];
	var thus = this;
	for (var i = 0; i < COORDS.length; i++){
		var area = document.createElement("AREA");
		area.shape = "rect";
		area.coords = COORDS[i].coords;
		(function(theKey) {area.onclick = function() {thus._pressKey(theKey);};})(COORDS[i].key);
		keyboardmap.appendChild(area); 
	}
};


// *** Utility
//Kram: Probably this is not so useful nowdays, but its better to be sure
JSTT.prototype.addEvent = function(obj, type, func){
	if (obj.attachEvent){
		obj.attachEvent("on" + type, func);
	}else if (obj.addEventListener){
		obj.addEventListener(type, func, false);
	}
};

/**
 * Export the class definition for the Closure Compiler
 */
window['JSTT'] = JSTT;
JSTT.prototype['printLine'] = JSTT.prototype.printLine;
JSTT.prototype['print'] = JSTT.prototype.print;
JSTT.prototype['carriageReturn'] = JSTT.prototype.carriageReturn;
JSTT.prototype['inkey'] = JSTT.prototype.inkey;