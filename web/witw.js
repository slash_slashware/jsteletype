var WITW = {};

WITW.init = function (jstt){
	WITW.jstt = jstt;
	WITW.title();
};

WITW.title = function(){
	WITW.jstt.print(" * * *   WHERE IN THE WORLD IS KRAMEN SANDIEGO    * * *");
	WITW.jstt.carriageReturn();
	WITW.jstt.print("WHAT IS YOUR NAME: ");
	WITW.jstt.inkey(function(name){
		WITW.playerName = name;
		WITW.setAge();
	});
};

WITW.setAge = function(){
	WITW.jstt.print("WELCOME "+WITW.playerName+", WHAT IS YOUR AGE? ");
	WITW.jstt.inkey(function(age){
		var ageNumber = parseInt(age);
		WITW.jstt.print("I AM "+(ageNumber+5)+", OLDER THAN YOU.");
	});
};
